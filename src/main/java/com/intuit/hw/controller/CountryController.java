package com.intuit.hw.controller;


import com.intuit.hw.contracts.CountryMapper;
import com.intuit.hw.dto.CountryForm;
import com.intuit.hw.model.Country;
import com.intuit.hw.service.CountryService;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.omg.CORBA.portable.Streamable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by cheikhniass on 12/9/15.
 */
@RestController
public class CountryController {
    private CountryService countryService;

    @Autowired
    public CountryController(CountryService countryService) {
        this.countryService = countryService;
    }

    public CountryController() {

    }


    @RequestMapping(value = "/add-country", method = RequestMethod.POST)
    public ResponseEntity<CountryForm> addCountry(@RequestBody CountryForm countryForm) {
        countryService.insert(countryForm);
        return new ResponseEntity<>(countryForm, HttpStatus.CREATED);
    }


    @RequestMapping(value = "/countries", method = RequestMethod.GET)
    public ResponseEntity<List<CountryMapper>> showAllCountries() {
        ResponseEntity<List<CountryMapper>> response = null;
        List<Country> countries = countryService.getAllCountries();
        if (countries == null)
            response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        else {
            List<CountryMapper> countryMappers = new ArrayList<>();
            for (Country c : countries) {
                countryMappers.add(mapCountry(c));
            }
            response = new ResponseEntity<>(countryMappers, HttpStatus.OK);
        }
        return response;

    }

    @RequestMapping(value = "/country", method = RequestMethod.GET)
    public ResponseEntity<CountryMapper> showSingleCountry(@RequestParam(value = "countryName", required = true,
            defaultValue = "Senegal")String countryName) {
        ResponseEntity<CountryMapper> response = null;
        try {
            Country foundCountry = countryService.getCountry(countryName);
            response = new ResponseEntity<>(mapCountry(foundCountry), HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            response = new ResponseEntity<>(new CountryMapper(), HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            response = new ResponseEntity<CountryMapper>(HttpStatus.INTERNAL_SERVER_ERROR);

        }

        return response;
    }


    public CountryMapper mapCountry(Country country) {
        CountryMapper countryMapper = new CountryMapper();
        countryMapper.setId(country.getId());
        countryMapper.setCountryName(country.getCountryName());
        countryMapper.setCountrySize(country.getCountrySize());
        return countryMapper;
    }

}
