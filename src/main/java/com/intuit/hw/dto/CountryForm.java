package com.intuit.hw.dto;

/**
 * Created by cheikhniass on 12/9/15.
 */
public class CountryForm {
    private int id;
    private String countryName;
    private double countrySize;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getCountrySize() {

        return countrySize;
    }

    public void setCountrySize(double countrySize) {
        this.countrySize = countrySize;
    }

    public String getCountryName() {

        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
