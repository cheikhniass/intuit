package com.intuit.hw.model;

/**
 * Created by cheikhniass on 12/9/15.
 */
public class Country {
    private int id;
    private String countryName;
    private double countrySize;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getCountrySize() {

        return countrySize;
    }

    public void setCountrySize(double countrySize) {
        this.countrySize = countrySize;
    }

    public String getCountryName() {

        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
    @Override
    public String toString() {
        return "Country{" +
                "countryName='" + countryName + '\'' +
                ", id=" + id +
                ", countrySize=" + countrySize +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Country country = (Country) o;

        if (id != country.id) return false;
        if (Double.compare(country.countrySize, countrySize) != 0) return false;
        return countryName.equals(country.countryName);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + countryName.hashCode();
        temp = Double.doubleToLongBits(countrySize);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
