package com.intuit.hw.repo;

import com.intuit.hw.model.Country;

import java.util.List;

/**
 * Created by cheikhniass on 12/9/15.
 */
public interface CountryRepo {


    void insert(Country country);

    List<Country> getAllCountries();

    Country getCountry(String country);
}
