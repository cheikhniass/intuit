package com.intuit.hw.repo;

import com.intuit.hw.model.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by cheikhniass on 12/9/15.
 */
@Repository
@Transactional
public class CountryRepoImpl implements CountryRepo {
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
     @Autowired
    public CountryRepoImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public void insert(Country country) {
        BeanPropertySqlParameterSource bean = new BeanPropertySqlParameterSource(country);
        String sql = "insert into country(countryName, populationSize) values(:countryName,:countrySize)";
        namedParameterJdbcTemplate.update(sql,bean);

    }

    @Override
    public List<Country> getAllCountries() {
        String query = "select * from country";
       return namedParameterJdbcTemplate.query(query, new RowMapper<Country>() {
            @Override
            public Country mapRow(ResultSet resultSet, int i) throws SQLException {
                Country country = new Country();
                country.setId(resultSet.getInt("id"));
                country.setCountryName(resultSet.getString("countryName"));
                country.setCountrySize(resultSet.getDouble("populationSize"));
                return country;
            }
        });
    }

    @Override
    public Country getCountry(String country) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("countryName",country);
        String query = "select * from country where countryName = :countryName";
        return namedParameterJdbcTemplate.queryForObject(query,params, new RowMapper<Country>() {
            @Override
            public Country mapRow(ResultSet resultSet, int i) throws SQLException {
                Country country = new Country();
                country.setId(resultSet.getInt("id"));
                country.setCountryName(resultSet.getString("countryName"));
                country.setCountrySize(resultSet.getDouble("populationSize"));
                return country;
            }
        });
    }

}
