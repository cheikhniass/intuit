package com.intuit.hw.service;

import com.intuit.hw.dto.CountryForm;
import com.intuit.hw.model.Country;

import java.util.List;

/**
 * Created by cheikhniass on 12/9/15.
 */
public interface CountryService {

    void insert(CountryForm countryForm);

    List<Country> getAllCountries();


    Country getCountry(String country);
}
