package com.intuit.hw.service;


import com.intuit.hw.dto.CountryForm;
import com.intuit.hw.model.Country;
import com.intuit.hw.repo.CountryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by cheikhniass on 12/9/15.
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CountryServiceImpl implements CountryService {
    private CountryRepo countryRepo;

    @Autowired
    public CountryServiceImpl(CountryRepo countryRepo) {
        this.countryRepo = countryRepo;
    }

    public CountryServiceImpl() {

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void insert(CountryForm countryForm) {
        Country c = new Country();
        c.setCountryName(countryForm.getCountryName());
        c.setCountrySize(countryForm.getCountrySize());
        countryRepo.insert(c);

    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Country> getAllCountries() {
        List<Country> countries= null;
        countries  = countryRepo.getAllCountries();
        return countries;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Country getCountry(String country) {
        Country singleCountry = null;
        singleCountry = countryRepo.getCountry(country);
        return singleCountry;

    }


}
